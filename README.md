# QA / Test Analyst assignment

***FAQ Page Stretch & Sweat***


FAQ page is a working feature at Megarush, designed to allow players to browse and search for answers related to gaming on our site. The answers are organised in categories of questions which player can navigate to and expand. The page itself is optimised for SEO so bots can index its content under specific URLs.

A user story for this feature could have looked like this:

*As a player I want to navigate to FAQ page at Megarush and browse through its content using subject categories horizontal menu and a list of questions within those categories. I want to be able to search for a phrase to get corresponding results; when search results are shown the categories menu should be hidden. When I expand a question for an answer, previously expanded questions remain expanded.  When I navigate to category or / and question, the URL of the page should reflect that location.*

Please come up with a good functional test coverage for the FAQ page, [https://www.megarush.com/en/faq](https://www.megarush.com/en/faq) minding that:

- the content (categories shown on the horizontal menu, as well as all the questions and answers) is dynamic and shouldn't be part of test data

- the search engine is a 3rd party library, most likely already covered with tests, and its configuration shouldn't be part of scope for this task

- test cases should be written with Gherkin

Please don't spend more than 2 afternoons on this task and try to deliver a result within a week. Have fun!